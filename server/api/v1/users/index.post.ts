const USERS = [
  {
    email: "jim@gmail.com",
    number: "221122"
  },
  {
    email: "jam@gmail.com",
    number: "830347"
  },
  {
    email: "john@gmail.com",
    number: "221122"
  },
  {
    email: "jams@gmail.com",
    number: "349425"
  },
  {
    email: "jams@gmail.com",
    number: "141424"
  },
  {
    email: "jill@gmail.com",
    number: "822287"
  },
  {
    email: "jill@gmail.com",
    number: "822286"
  }
]

export default defineEventHandler(async (event) => {
  const {
    email,
    number
  } = await readBody<{
    email: string,
    number: string
  }>(event, {
    strict: true
  })

  return await new Promise((resolve) => {
    setTimeout(() => {
      resolve(
        USERS.filter((user) => {
          return (
            user.email.includes(email) &&
            user.number.includes(number)
          )
        })
      )
    }, 5_000)
  })
})
