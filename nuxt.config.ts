export default defineNuxtConfig({
  devtools: {
    enabled: true
  },
  modules: [
    [
      "@vee-validate/nuxt",
      {
        autoImports: true
      }
    ]
  ],
  serverDir: "./server/",
  srcDir: "./src/",
  ssr: false
})
